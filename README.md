# App Ideas

Random ideas for new apps (or system features) that would be nice to have in the GNOME ecosystem.

**Note: This repository is intentionally empty, the ideas are in issues :)**